google.charts.load('current', {packages: ['corechart']})
google.charts.setOnLoadCallback(drawChart)
function drawChart () {
  var data = google.visualization.arrayToDataTable([
    ['Element', 'Density', { role: 'style' } ],
    ['Jesus', 100, '#0d47a1'],
    ['Moy', 90, '#1a237e'],
    ['Pastrana', 80, '#311b92 '],
    ['Rebeca', 70, '#1565c0']
  ])

  var view = new google.visualization.DataView(data)
  view.setColumns([0, 1, {
    calc: 'stringify',
    sourceColumn: 1,
    type: 'string',
    role: 'annotation' },
    2])

  var options = {
    title: 'Scores',
    width: '100%',
    height: '100%',
    bar: {groupWidth: '95%'},
    legend: { position: 'none' }
  }
  var chart = new google.visualization.BarChart(document.getElementById('barchart_values'))
  chart.draw(view, options)
}
