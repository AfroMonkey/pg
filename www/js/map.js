function initMap () {
  var directionsService = new google.maps.DirectionsService
  var directionsDisplay = new google.maps.DirectionsRenderer
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 13,
    center: {lat: 20.6539887, lng: -103.3279475}
  })
  directionsDisplay.setMap(map)
  document.getElementById('search').addEventListener('click', function () { calculateAndDisplayRoute(directionsService, directionsDisplay) })
}

function calculateAndDisplayRoute (directionsService, directionsDisplay) {
  directionsService.route({
    origin: document.getElementById('start').value,
    destination: document.getElementById('end').value,
    travelMode: 'DRIVING'
  }, function (response, status) {
    if (status === 'OK') {
      directionsDisplay.setDirections(response)
    } else {
      window.alert('Directions request failed due to ' + status)
    }
  })
}
